<?php
/*
    Plugin Name: NASA_astronautas
    Plugin URI: https://nasa-landing-astronautas
    Description: plugin para agregar astronautas
    shortcode [NASA_astronautas]
    Version:1.0
    Author:Gradiweb
    Author URI:https://gradiweb.com
    License:GPL2
*/
    //hook de activacion
        register_activation_hook(__FILE__, 'Nasa_astronautas_init');

        function Nasa_astronautas_init() 
        {
            global $wpdb;
            $tabla_astronautas = $wpdb->prefix . '_NASA_astronautas';
            $charset_collate = $wpdb->get_charset_collate();
            //consultar para crear la tabla en la DB
            $query = "CREATE TABLE IF NOT EXISTS $tabla_astronautas (
                nombre varchar(50) NOT NULL,
                correo varchar(50) NOT NULL,
                edad integer(10) NOT NULL,
                motivo varchar (200) NOT NULL,
                contacto varchar(200) NOT NULL,
                sexo varchar(200) NOT NULL,
                created_at datetime NOT NULL
            ) $charset_collate";
        include_once ABSPATH . 'wp-admin/includes/upgrade.php';
        dbDelta($query);
        }

    //define el shortcode que pinta el formulario
    add_shortcode('NASA_astronautas','NASA_astronautas');

    function NASA_astronautas() {

        global $wpdb;
        
        if (!empty($_POST)
            AND $_POST['nombre'] != ''
            AND is_email($_POST['correo'])
            AND $_POST['sexo'] != ""
            AND $_POST['motivo'] != ""
            AND $_POST['contacto'] != ""
            AND $_POST['edad'] != ""
        ) {
            $tabla_astronautas = $wpdb->prefix . '_NASA_astronautas';
            $nombre = sanitize_text_field($_POST['nombre']);
            $correo = sanitize_text_field($_POST['correo']);
            $sexo = sanitize_text_field($_POST['sexo']);
            $motivo = sanitize_text_field($_POST['motivo']);
            $contacto = sanitize_text_field($_POST['contacto']);
            $edad = (int)$_POST['edad'];
            $created_at = date('Y-m-d H:i:s');

            $wpdb->insert(
                $tabla_astronautas, 
                 array(
                     'nombre' => $nombre,
                     'correo' => $correo,
                     'sexo' => $sexo,
                     'motivo' => $motivo,
                     'contacto' => $contacto,
                     'edad' => $edad,
                     'created_at' => $created_at,            
                     )
            );
        }
        wp_enqueue_style('css_astronautas', plugins_url('style.css', __FILE__));
    
        ob_start();
        ?>
            <form action="<?php get_the_permalink() ?>" method="post" class="newAstronauta">
            <?php wp_nonce_field('save_astronauta', 'astronauta_nonce'); ?>
                <div class="form-input">
                    <h6>Nombre Completo</h6>
                    <input type="text" name="nombre" required="required" placeholder="Nombre Completo">
                </div>
                <div class="form-input">
                    <h6>Edad</h6>
                    <input type="text" name="edad" required="required" placeholder="Edad">
                </div>
                <div class="form-input">
                    <h5>Sexo</h5>
                    <input type="text" name="sexo" required="required" placeholder="Sexo">
                </div>
                <div class="form-input">
                    <h6>Correo Electronico</h6>
                    <input type="email" name="correo" required="required" placeholder="ejemplo@.com">
                </div>
                <div class="form-input">
                    <h6>¿Que te motiva a ir a la luna?</h6>
                    <textarea name="motivo" rows="4" cols="50"></textarea>
                </div>
                <div class="form-input">
                    <h6>¿Cuando fue tu ultimo contacto con extraterrestres?</h6>
                    <textarea name="contacto" rows="4" cols="50"></textarea>
                </div>
                <div>
                    <input type="submit" class="button" value="enviar">
                </div>
                
            </form>
        <?php
        return ob_get_clean();
    }

    add_action("admin_menu", "Astronauta_menu");
    /**
     * Agrega el menú del plugin al formulario de WordPress
     *
     * @return void
     */
    function Astronauta_menu() {
        add_menu_page("Formulario astronautas", "NASA clientes", "manage_options",
            "astronauta_menu", "nasa_candidatos_admin", "dashicons-feedback", 75);
    }

    function nasa_candidatos_admin() {
        global $wpdb;
        $tablaAstronautas = $wpdb->prefix . '_NASA_astronautas';
        $astronautas = $wpdb->get_results("SELECT * FROM $tablaAstronautas");
        echo '<div class="wrap"><h1>Candidatos Astronautas</h1>';
        echo '<table class="wp-list-table widefat fixed striped">';
        echo '<thead><tr><th width="30%">Nombre</th><th width="20%">edad</th>';
        echo '<th width="30%">Sexo</th><th width="20%">Correo</th><th width="20%">Motivacion</th><th width="20%">Contacto</th>';
        echo '</tr></thead>';
        echo '<tbody id="the-list">';
        foreach ($astronautas as $astronauta) {
            $nombre = esc_textarea($astronauta->nombre);
            $edad = (int) $astronauta->edad;
            $sexo = esc_textarea($astronauta->sexo);
            $correo = esc_textarea($astronauta->correo);
            $motivo = esc_textarea($astronauta->motivo);
            $contacto = esc_textarea($astronauta->contacto);
            
            echo "<tr><td><a href='#' title='$motivo'>$nombre</a></td>";
            echo "<td>$edad</td><td>$sexo</td><td>$correo</td>";
            echo "<td>$motivo</td><td>$contacto</td>";
        }
        echo '</tbody></table></div>';

    }
?>